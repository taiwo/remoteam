# Remoteam - share notes, chat and hold meetings anywhere

A multi-tenant application where remote teams can keep notes, chat and hold virtual meetings.. You can view a live version [here](https://remoteam.herokuapp.com/).

## Table of contents

- [Technologies](#technologies)
- [Setup](#setup)
- [Features](#features)
- [Status](#status)
- [Test suite](#test-suite)
- [CI/CD](#ci/cd)

## Technologies

### Backend

- Ruby - 2.7.0p0
- Rails - 6.0.3.4
- Database - PostgreSQL

### Frontend

- Bulma
- Javascript

### TestSuite

- RSpec
- Faker
- FactortBot

## Setup

- Clone the project on your machine using: `git clone https://git.heroku.com/remoteam.git`
- Run `bundle install` to install needed gems
- Run `rails db:setup` to setup the database
- Launch the app using `rails server`
- Visit `http://localhost:3000/` in your browser

## Features/To-do list


To-do list:
 
- Multi-tenancy
- User authentication and authorization
- Note sharing
- Chats room
- Virtual meetings

## Status

Project is: _in progress_

## Test suite

- You can run the test by running `bundle exec rspec`

## CI/CD
- To-do
