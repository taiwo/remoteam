# frozen_string_literal: true

Rails.application.routes.draw do
  get 'dashboard', to: 'static_pages#dashboard'
  root to: 'static_pages#home'
end
